'use strict';

module.exports = {
	env: {
		node: true
	},
	extends: [
		'wikimedia',
		'wikimedia/node',
		'wikimedia/language/es2022'
	],
	overrides: [
		{
			files: [
				'.eslintrc.{js,cjs}'
			],
			parserOptions: {
				sourceType: 'script'
			}
		}
	],
	parserOptions: {
		ecmaVersion: 'latest'
	},
	rules: {
	}
};
