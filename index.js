'use strict';

const axios = require( 'axios' );
const axiosRetry = require( 'axios-retry' ).default;

/**
 * Check is status code is redirect
 *
 * @param {number} status Status code
 * @return {boolean}
 */
const isRedirect = ( status ) => status < 400 && status >= 300;

/**
 * Make outgoing HTTP request using axios
 *
 * @param {Object} outReq Outgoing request
 * @param {Object} parentReq Incoming request
 * @return {Promise} outgoing request config
 */
function makeOutgoingRequest( outReq, parentReq ) {
	const httpConf = parentReq.app.conf.http_client || {};

	// Log each outgoing request on incoming request logger
	const loggerInterceptor = ( config ) => {
		parentReq.logger.log( 'trace/req', { msg: 'Outgoing request', outRequest: outReq } );
		return config;
	};

	// Handle service mesh redirects
	const serviceMeshInterceptor = ( ( response ) => {
		if ( isRedirect( response.status ) ) {
			const location = response.headers.location;
			try {
				// Redirect location is absolute
				const nextURL = new URL( response.headers.location );
				response.config.url.pathname = nextURL.pathname;
				response.config.headers.host = nextURL.hostname;
			} catch ( err ) {
				// Redirect location is relative
				response.config.url = new URL( location, response.config.url );
			}
			return axios.request( response.config );
		}
		return response;
	} );

	Object.assign( outReq.headers, {
		'user-agent': parentReq.app.conf.user_agent,
		'x-request-id': parentReq.headers[ 'x-request-id' ]
	} );

	const reqConfig = {
		url: new URL( outReq.uri ),
		method: outReq.method,
		headers: outReq.headers,
		timeout: httpConf.timeout || 2 * 60 * 1000,
		validateStatus: httpConf.status_validator || ( ( status ) => status < 400 )
	};

	if ( outReq.body ) {
		reqConfig.data = outReq.body;
	}

	// Ensure requests are retried in case of failure
	axiosRetry( axios, { retries: httpConf.retries || 3 } );

	axios.interceptors.request.use( loggerInterceptor );
	if ( httpConf.use_service_mesh ) {
		// Manually handle redirects for service mesh
		reqConfig.maxRedirects = 0;
		axios.interceptors.response.use( serviceMeshInterceptor );
	}

	return axios.request( reqConfig );
}

module.exports = {
	makeOutgoingRequest
};
